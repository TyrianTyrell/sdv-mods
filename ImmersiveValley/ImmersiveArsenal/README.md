<table align="center"><tr><td align="center" width="9999">

<!-- LOGO, TITLE, DESCRIPTION -->

# Arsenal - Immersive Weapons

<br/>

<!-- TABLE OF CONTENTS -->
<details open="open" align="left">
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#features">Features</a></li>
    <li><a href="#compatibility">Compatbility</a></li>
    <li><a href="#installation">Installation</a></li>
    <li><a href="#special-thanks">Special Thanks</a></li>
    <li><a href="#license">License</a></li>
  </ol>
</details>

</td></tr></table>

## Features

This mod brings the following immersive changes to vanilla weapons:

- Rebalances the weapons stats, creating a more natural progression between the many available weapons and emphasizing the advantages and disadvantages of each weapon type, creating more unique gameplay opportunities.
- Rebalances the Jade, Topaz and Vampiric enchants:
    - **Jade:** *+10% -> +50% crit. power per level.* Vanilla 10% crit. power grants a 10% damage bonus that only applies to crits. This makes it significantly worse than the Ruby enchantment, which grants the same 10% bonus, but all the time. At 50% crit. power, the Jade ring becomes a better choice than the Ruby ring when crit. chance is at least 20%. It should also be high enough to consider over more crit. chance.
    - **Topaz:** *+1 -> +5 defense per level.* A much needed buff to make this a viable alternative for defensive builds.
    - **Vampiric:** *9% chance to steal ~9% of monster health on kill.* -> *Drains 1% health on-hit, but steals ~5% of monster health on kill.* The original 9% chance made this enchantment too unreliable. The new enchantment is considerably more useful, and more interesting.
- Weapons cost stamina. Because why wouldn't they? This adds another strategic element to combat which also helps to balance different weapon types; as you can no longer spam your weapon, you must ponder whether to maximize the damage of a single heavy hit, or rely on quick light stabs to land critical hits. Same formula as other tools, with stamina cost decreasing with Combat level.
- Removes slingshot grace period, which in vanilla causes slingshot projectiles to ignore targets that get too close. This essentially made slingshots unreliable as a main weapon. Well, no more!
- Removes the 50% damage mitigation soft cap from vanilla. This significantly dimished the value of defense bonuses and defensive builds. Now, you can stack resistance until the hard cap, which is 1 point of damage per hit received.
- Replaces the Rusty Sword in Marlon's introduction event with a Wooden Blade. It makes much more sense for Marlon to have training Wooden Blades lying around. The Wooden Blade was in an awkward position in vanilla, where it was supposedly an upgrade to a rusty blade of steel.
- Changed the conditions for obtaining the Galaxy Sword. It always bugged me that getting a single lucky Prismatic Shard early on (which is not too difficult either in the regular Mines or fishing chests) almost immediately got you the best weapon in the game, without even needing to set foot in the Skull Caverns. So I "fixed" that. The Galaxy Sword now requires a different, more special item, normally obtainable only post-Ginger Island. But maybe someone can give it to you earlier.
    - There's also a fitting new riddle for the Pelican Town graveyard. Be sure to check it out.

All features can toggled on or off.

## Compatibility

Not compatible with other rebalance mods.

Compatible with Stardew Valley Expanded.

Should be fully compatible with multiplayer. Not compatible with Android.

## Installation

Install like any other mod, by extracting the content of the downloaded zip file to your mods folder and starting the game via SMAPI.

To update, first delete the old version and then install the new one. You can optionally keep your configs.json in case you have personalized settings.

To uninstall simply delete the mod from your mods folder. This mod is safe to uninstall at any point.

## Special Thanks

- **ConcernedApe** for StardewValley.
- [JetBrains](https://jb.gg/OpenSource) for providing a free license to their tools.

<table>
  <tr>
    <td><img width="64" src="https://smapi.io/Content/images/pufferchick.png" alt="Pufferchick"></td>
    <td><img width="80" src="https://resources.jetbrains.com/storage/products/company/brand/logos/jb_beam.svg" alt="JetBrains logo."></td>
  </tr>
</table>

## License

See [LICENSE](../../LICENSE) for more information.
