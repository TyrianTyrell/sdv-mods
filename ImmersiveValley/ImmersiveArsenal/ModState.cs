﻿namespace DaLion.Stardew.Arsenal;

internal class ModState
{
    internal int SlingshotCooldown { get; set; }
    internal int EnergizeStacks { get; set; } = -1;
}