﻿namespace DaLion.Stardew.Arsenal;

public class Constants
{
    public const int DARK_SWORD_INDEX_I = 2,
        HOLY_BLADE_INDEX_I = 3,
        GALAXY_SWORD_INDEX_I = 4,
        WOODEN_BLADE_INDEX_I = 12,
        SNOWBALL_PROJECTILE_INDEX_I = 6,
        QUINCY_PROJECTILE_INDEX_I = 14,
        MASTER_SLINGSHOT_INDEX_I = 33,
        GALAXY_SLINGSHOT_INDEX_I = 34,
        INFINITY_BLADE_INDEX_I = 62,
        INFINITY_DAGGER_INDEX_I = 63,
        INFINITY_CLUB_INDEX_I = 64,
        PRISMATIC_SHARD_INDEX_I = 74,
        IRIDIUM_BAR_INDEX_I = 337,
        GALAXY_SOUL_INDEX_I = 896,
        SLINGSHOT_COOLDOWN_TIME_I = 2000;
}