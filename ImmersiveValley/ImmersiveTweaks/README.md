<table align="center"><tr><td align="center" width="9999">

<!-- LOGO, TITLE, DESCRIPTION -->

# Quality Of Life - Immersive Tweaks

<br/>

<!-- TABLE OF CONTENTS -->
<details open="open" align="left">
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#features">Features</a></li>
    <li><a href="#compatibility">Compatbility</a></li>
    <li><a href="#installation">Installation</a></li>
    <li><a href="#special-thanks">Special Thanks</a></li>
    <li><a href="#license">License</a></li>
  </ol>
</details>

</td></tr></table>

## Features

This mod is a collection of small tweaks and fixes to any vanilla inconsistencies or balancing issues that I can find, and that are not large enough to merit their own mod.
Most of these features were originally a part of [Walk Of Life](https://www.nexusmods.com/stardewvalley/mods/8111) and therefore are intended to be used with it, but that is not a requirement.

Features currently included:

(Foraging and Farming-related)
- Makes Bee House, Mushroom Box and Tapper produce improve quality with age, following the example of Fruit Trees. NEW: Quality also considers skill levels.
- Makes Mushroom Box, Tappers and Berry Bushes reward foraging experience when harvested.
- Makes large Eggs and Milk double the output stack instead of increasing the quality.
- Prevents Fruit Trees from growing in winter.
- Allow Botanist/Ecologist perk to apply to hoed Ginger and Coconuts obtained from shaking palm trees.
- Makes Meads take after the flower type of the input Honey item.
- Causes bombs to explode immediately if in range of another explosion.

All features can be toggled on or off.

## Compatibility

This mod makes use of Harmony to patch several vanilla behaviors. Any SMAPI mods with similar features are obviously **not** compatible.

- [Forage Fantasy](https://www.nexusmods.com/stardewvalley/mods/7554) has several conflicting features﻿, but can be used together with this mod if you pay attention to the configs of both mods.
- Recommended to pair with [Better Artisan Good Icons](https://www.nexusmods.com/stardewvalley/mods/2080) and [Better Artisan Good Icons - Mead Addon](https://www.nexusmods.com/stardewvalley/mods/11911)
Should be fully compatible with multiplayer. Not compatible with Android.

## Installation

Install like any other mod, by extracting the content of the downloaded zip file to your mods folder and starting the game via SMAPI.

To update, first delete the old version and then install the new one. You can optionally keep your configs.json in case you have personalized settings.

To uninstall simply delete the mod from your mods folder. This mod is safe to uninstall at any point.

## Special Thanks

- **ConcernedApe** for StardewValley.
- [JetBrains](https://jb.gg/OpenSource) for providing a free license to their tools.

<table>
  <tr>
    <td><img width="64" src="https://smapi.io/Content/images/pufferchick.png" alt="Pufferchick"></td>
    <td><img width="80" src="https://resources.jetbrains.com/storage/products/company/brand/logos/jb_beam.svg" alt="JetBrains logo."></td>
  </tr>
</table>

## License

See [LICENSE](../../LICENSE) for more information.
