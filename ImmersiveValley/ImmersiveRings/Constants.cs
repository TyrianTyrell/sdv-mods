﻿namespace DaLion.Stardew.Rings;

public class Constants
{
    public const int
        // gemstones
        EMERALD_INDEX_I = 60,
        AQUAMARINE_INDEX_I = 62,
        RUBY_INDEX_I = 64,
        AMETHYST_INDEX_I = 66,
        TOPAZ_INDEX_I = 68,
        JADE_INDEX_I = 70,

        // rings
        SMALL_GLOW_RING_INDEX_I = 516,
        GLOW_RING_INDEX_I = 517,
        SMALL_MAGNET_RING_INDEX_I = 518,
        MAGNET_RING_INDEX_I = 519,
        IRIDIUM_BAND_INDEX_I = 527,
        AMETHYST_RING_INDEX_I = 529,
        TOPAZ_RING_INDEX_I = 530,
        AQUAMARINE_RING_INDEX_I = 531,
        JADE_RING_INDEX_I = 532,
        EMERALD_RING_INDEX_I = 533,
        RUBY_RING_INDEX_I = 534,
        CRAB_RING_INDEX_I = 810,
        GLOWSTONE_RING_INDEX_I = 888,

        // other
        SUN_ESSENCE_INDEX_I = 768,
        VOID_ESSENCE_INDEX_I = 769;
}