﻿namespace DaLion.Stardew.Professions.Framework.TreasureHunts;

public enum TreasureHuntType
{
    Scavenger,
    Prospector
}