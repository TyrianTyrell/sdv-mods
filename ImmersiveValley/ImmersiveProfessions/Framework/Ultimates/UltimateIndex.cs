﻿namespace DaLion.Stardew.Professions.Framework.Ultimates;

public enum UltimateIndex
{
    None = -1,
    BruteFrenzy = 26,
    PoacherAmbush = 27,
    PiperConcerto = 28,
    DesperadoBlossom = 29
}