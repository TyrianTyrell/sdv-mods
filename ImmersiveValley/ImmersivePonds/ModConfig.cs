﻿namespace DaLion.Stardew.Ponds;

/// <summary>The mod user-defined settings.</summary>
internal class ModConfig
{
    /// <summary>Multiplies a fish's base chance to produce roe each day.</summary>
    public float RoeProductionChanceMultiplier = 1f;

    /// <summary>Number of days until an empty pond will begin spawning algae.</summary>
    public int DaysUntilAlgaeSpawn = 2;
}