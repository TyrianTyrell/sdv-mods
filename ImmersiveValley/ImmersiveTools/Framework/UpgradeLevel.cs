﻿namespace DaLion.Stardew.Tools.Framework;

public enum UpgradeLevel
{
    None,
    Copper,
    Steel,
    Gold,
    Iridium,
    Radioactive,
    Mythicite,
    Enchanted
}